#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk
from time import sleep
import splash
import functions

version="0.4"

class Main:
    def __init__(self):
        gladeFile = "/home/henry/code/gtk-code/assistant/assistant.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladeFile)
        self.builder.connect_signals(self)

        # Creates a splash screem
        splScr = splash.SplashScreen()

        # It will not destroy the splash screen until the job has done.
        while gtk.events_pending():
            gtk.main_iteration()

        self.welcome_text()
        sleep(4)

        # Destroy the splash screen
        splScr.destroy()

        # Pack Window
        window = self.builder.get_object("main")
        window.connect("delete-event", gtk.main_quit)
        window.show()

    def on_click_restart(self, widget):
        print("Restart button is clicked")
        # show_in_app_notification(self, "Restarting")

    def on_click_quit(self, widget):
        print("\n[*] Exiting Arcigo Assistant...\n")
        gtk.main_quit()

    def on_click_reload(self, widget):
        print("Reload button is clicked")

    def exit_message(self, widget, another_dump):
        print("\n[*] Exiting Arcigo Assistant...\n")

    def welcome_text(self):
        print("\n\t============= ARCIGO ASSISTANT =============-\n")
        sleep(0.5)
        print("[*] Gitlab: https://gitlab.com/arcigo-linux/arcigo-assistant")
        print(f"[*] Version: {version}")
        print("[*] Info: This tool is under development")
        print(f"[*] PID: {functions.os.getpid()}")
        print(f"[*] User: {functions.username}")
        print(f"[*] Distro: {functions.distro_name}")
        print("[*] Config will get backed up !")
        print("[*] Some tabs might not work !")
        print(f"[*] Log will be maintained at '{functions.log_dir}'")
        print(f"[*] Config will be maintained at '{functions.config_dir}'")
        sleep(1)
        print("[*] Starting Arcigo Assistant...\n")
        functions.show_notify("Arcigo Assistant", "Running", "dialog-information")
        sleep(1.5)

if __name__ == '__main__':
    main = Main()
    try:
    	gtk.main()
    except KeyboardInterrupt:
    	print("\n[*] Exiting Arcigo Assistant...\n")
