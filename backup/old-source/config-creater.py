    #!/usr/bin/env python3

# Config Creater
# It can create, append, write and read the configs.
# Author: Henry

from configparser import ConfigParser
import os
from tempfile import tempdir
from threading import local
from functions import home_dir, config

# Config Location
# config_lc = config
config_lc = './settings.ini'

parser = ConfigParser()

def make_file(section, key):
    parser[section] = key
    with open(config_lc, 'w') as configfile:
        parser.write(configfile)


def new_settings(section, key):
    parser.read(config_lc)
    parser[section] = key

    with open(config_lc, 'w') as configfile:
        parser.write(configfile)


def write_settings(section, key, value):
    parser.read(config_lc)

    parser[section][key] = value
    with open(config_lc, 'w') as configfile:
        parser.write(configfile)


def read_section():
    file = parser.read(config_lc)
    return parser.sections()


def read_settings(section, key):
    parser.read(config_lc)

    return parser[section][key]

def append_section(section, key, value):
    # To append a section and key values to existing file
    
    with open(config_lc, 'r') as configfile:
        lines = configfile.readlines()
        configfile.close()
    str_match = '[' + section + ']'
    
    for i in lines:
        val = i.replace("\n", "")

        # If the args section is already in file it won't allow to append it
        if val == str_match:
            print(f"The section {str_match} is already used !")
            return False

    parser.add_section(section)
    parser.set(section, key, value)
    with open(config_lc, 'a') as configfile:
        configfile.write("\n")
        parser.write(configfile)
        configfile.close()

def _get_position(lists, value):
    data = [string for string in lists if value in string]
    position = lists.index(data[0])
    return position

def append_key_value(section):
    # To append key value to existing section
    with open(config_lc, 'r') as configfile:
        lines = configfile.readlines()
        configfile.close()
    str_match = '[' + section + ']'
    
    for i in lines:
        val = i.replace("\n", "")

        # If the args section is already in file it won't allow to append it
        if val == str_match:
            print(f"The section {str_match} is found !")
            new_val = val
            
            pos = _get_position(lines, "[custom]")
            num = pos+3
            
            lines.insert(num, "\n" + "textinfo" + "\n")
    
            print(lines)
            print("----------")
            # return True
        print(val)
        # print(i)

import functions
def insert_repo(self, text):
    with open(functions.pacman_config, "r") as f:
        lines = f.readlines()
        f.close()
    pos = _get_position(lines, "[custom]")
    num = pos+3

    lines.insert(num, "\n" + text + "\n")

    with open(functions.pacman_config, "w") as f:
        f.writelines(lines)
        f.close()


append_key_value("nemesis_repo")