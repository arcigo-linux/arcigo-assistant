#!/usr/bin/env python3

# ARCIGO ASSISTANT

# Author: Henry

import os
from tabnanny import check
import distro
import sys
import shutil
import psutil
import datetime
# import time
import subprocess


arcigo_repo = ["[arcigo-repo]", "SigLevel = Optional TrustedOnly", "Include = /etc/pacman.d/arcigo-mirrorlist"]

chaotics_repo = ['[chaotic-aur]', 'SigLevel = Required DatabaseOptional', 'Include = /etc/pacman.d/chaotic-mirrorlist']

endeavouros_repo = ['[endeavouros]', 'SigLevel = PackageRequired', 'Include = /etc/pacman.d/endeavouros-mirrorlist']

nemesis_repo = ['[nemesis_repo]', 'SigLevel = Optional TrustedOnly', 'Server = https://erikdubois.github.io/$repo/$arch']

arch_testing_repo = ['[testing]', 'Include = /etc/pacman.d/mirrorlist']

arch_core_repo = ['[core]', 'Include = /etc/pacman.d/mirrorlist']

arch_extra_repo = ['[extra]', 'Include = /etc/pacman.d/mirrorlist']

arch_community_testing_repo = ['[community-testing]', 'Include = /etc/pacman.d/mirrorlist']

arch_community_repo = ['[community]', 'Include = /etc/pacman.d/mirrorlist']

arch_multilib_testing_repo = ['[multilib-testing]', 'Include = /etc/pacman.d/mirrorlist']

arch_multilib_repo = ['[multilib]', 'Include = /etc/pacman.d/mirrorlist']

# Create a file
# f = open("pacman.conf", "w")
# f.write(arcigo_repo)


# Read a file

# f = open("pacman.conf", "r")
# arr = f.read().split('\n')

# while("" in arr):
#     arr.remove("")

# if arcigo_repo == arr:
#     print("Both list values are same\n")
# else:
#     print("list is not matching")
#     # exit(1)

# print("\nArray of `arr`")
# for i in arr:
#     print(i)

# print("\nArray of `arcigoo_repo`")
# for ar in arcigo_repo:
#     print(ar)

# f.close()



import functions

# def check_repo(value):
#     with open(functions.pacman_config, "r") as myfile:
#         lines = myfile.readlines()
#         myfile.close()

#     for line in lines:
#         if value in line:
#             if "#" + value in line:
#                 return False
#             else:
#                 return True
#     return False

# check_repo(arcigoo_repo)

def append_repo(repo):
    with open(functions.pacman_config, "a") as myfile:
        myfile.write("\n")
        for line in repo:
            myfile.write("\n")
            myfile.write(str(line))
        myfile.close()

# append_repo(endeavouros_repo)


def checking_repo(repo):
    with open(functions.pacman_config, "r") as myfile:
        lines = myfile.readlines()
        myfile.close()
        
    while("\n" in lines):
        lines.remove("\n")

    print("\nBefore:\n")
    print(lines)
    
    print("\n\n\n")
    
    for line in lines:
        print(line)
    
    new_lines = ''
    for char in lines:
        if char != '\n':
            new_lines += char

    print(new_lines)
    
    
    # if new_lines == repo[0]:
    #     print("Success")

# checking_repo(arcigo_repo)

def check_repo(repo):
    
    value = str(repo[0])
    
    with open(functions.pacman_config, "r") as myfile:
        lines = myfile.readlines()
        myfile.close()

    for line in lines:
        if value in line:
            print(line)
            if "#" + value in line:
                print("Yes it found , but commented out")
                return False
            else:
                print(f"True, that {value} repository is in pacman config")
                return True
    print(f"There is no {value} repository in pacman config !")
    return False

check_repo(arcigo_repo)