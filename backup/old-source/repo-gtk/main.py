#!/usr/bin/env python

# App: Manage pacman repositories
# Sunday 17 July 2022 06:20:00 PM IST

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk

class Main:
    def __init__(self):
        # super().__init__(title="Manage Pacman Repositories")
        gladeFile = "main.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladeFile)
        self.builder.connect_signals(self)
        
        window = self.builder.get_object("Main_Window")
        window.connect("delete-event", gtk.main_quit)
        window.show()

    def add(self):
        pass

if __name__ == '__main__':
    main = Main()
    gtk.main()
