import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def onButtonShow(self):
    revealer.set_reveal_child(True)

def onButtonClose(self):
    revealer.set_reveal_child(False)

builder = Gtk.Builder()
builder.add_from_file("test.glade")

window = builder.get_object("window1")

buttonShow = builder.get_object("button1")
buttonClose = builder.get_object ("button2")
revealer = builder.get_object("revealer2") 

buttonShow.connect ("clicked", onButtonShow)
buttonClose.connect ("clicked", onButtonClose)
window.connect ("destroy", Gtk.main_quit)
window.show_all()

Gtk.main()
