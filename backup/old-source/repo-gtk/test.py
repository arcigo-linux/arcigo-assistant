#!/usr/bin/env python


pacman_config = "myfile.txt"

arcigo_repo = ["[arcigo-repo]", "SigLevel = Optional TrustedOnly", "Include = /etc/pacman.d/arcigo-mirrorlist"]

chaotics_repo = ['[chaotic-aur]', 'SigLevel = Required DatabaseOptional', 'Include = /etc/pacman.d/chaotic-mirrorlist']


# def append_repo(repo):
#     with open(pacman_config, "a") as myfile:
#         myfile.write("\n")
#         for line in repo:
#             myfile.write(str(line))
#             myfile.write("\n")
#         myfile.close()

# append_repo(chaotics_repo)

def repo_in_config(repo):
    
    value = str(repo[0])
    
    with open(pacman_config, "r") as myfile:
        lines = myfile.readlines()
        myfile.close()

    for line in lines:
        if value in line:
            # print(line)
            if "#" + value in line:
                # print("Yes it found , but commented out")
                return "commented"
            else:
                print(f"True, that {value} repository is in pacman config")
                return True
    print(f"There is no {value} repository in pacman config !")
    return "no config"

# print(repo_in_config(chaotics_repo))


def uncomment_repo(repo):
	value = str(repo[0])
	
	if repo_in_config(repo) == "commented":
	
		with open(pacman_config, "r") as myfile:
			lines = myfile.readlines()
			myfile.close()

		for line in lines:
			if value in line:
				if "#" + value in line:
					# print("Yes it is commented out")
					# somas = lines
					# lines.remove("#[chaotic-aur]\n")
					name = "#" + value
					indexofhead = lines.index(f"{name}\n")
					print("")
				
					i = 0
					while i<3:
						print(lines.pop(indexofhead).strip().replace("#", ""))
						i += 1

					# Do things to un comment
				else:
					print(f"There is no {1:repo} repository in pacman config")
					return False

# uncomment_repo(chaotics_repo)

###
# To Check that how to remove certain strings in a file in open mode
###


def mera():
	with open(pacman_config, "r") as rfile, \
		 open("newfile.txt", 'w') as wfile:
	    data = rfile.read()
	    data = data.replace("#", "")
	    wfile.write(data)

# mera()

with open("pacman.conf", "r") as rfile:
	data = rfile.read()
	# print(data)

# if not ("#") in data





# chaotic_aur = ['#[chaotic-aur]\n', '#SigLevel = Required DatabaseOptional\n', '#Include = /etc/pacman.d/chaotic-mirrorlist\n']

# print(chaotic_aur)

# for i in chaotic_aur:
# 	print(i[1:len(i)-1])
