#!/usr/bin/env python3

# Old source
import os
from turtle import home
import distro
import sys
import shutil
import psutil

# import time
import subprocess

# -------------- Info --------------

distro_name = distro.id()
username = os.getlogin()
home_dir = os.path.expanduser("~")
config = home_dir + "/.config/arcigo-assistant/settings.ini"
config_dir = home_dir + "/.config/arcigo-assistant/"
# pacman_config = "/etc/pacman.conf"
pacman_config = "./pacman.conf"
arcigo_mirrorlist = "/etc/pacman.d/arcigo-mirrorlist"
arcigo_mirrorlist_original = "/usr/share/arcigo-linux/arcigo-mirrorlist"
pacman_eos ="/usr/share/arcigo-linux/data/eos/pacman.conf"
pacman_garuda ="/usr/share/arcigo-linux/data/garuda/pacman.conf"
blank_pacman_arco ="/usr/share/arcigo-linux/data/arco/blank/pacman.conf"
blank_pacman_eos ="/usr/share/arcigo-linux/data/eos/blank/pacman.conf"
blank_pacman_garuda ="/usr/share/arcigo-linux/data/garuda/blank/pacman.conf"
autostart = home_dir + "/.config/autostart/"


##########################################
#               Functions
##########################################

def read_repo():
    pass

def write_repo():
    pass

def _get_position(lists, value):
    data = [string for string in lists if value in string]
    position = lists.index(data[0])
    return position

###############################
#    If file or path exists
###############################
def file_check(file):
    if os.path.isfile(file):
        return True

    return False


def path_check(path):
    if os.path.isdir(path):
        return True

    return False

###############################
#    Runs as other user
###############################
def run_as_user(script):
    subprocess.call(["su - " + username + " -c " + script], shell=False)


###############################
#    Create a log file
###############################
log_dir="/var/log/arcigo-linux/"

def create_log():
    print('Making log in ' + str(log_dir))
    now = datetime.datetime.now()
    time = now.strftime("%Y-%m-%d-%H-%M-%S" )
    destination = log_dir + 'arcigo-assistant-log-' + time
    command = 'sudo pacman -Q > ' + destination
    subprocess.call(command,
                    shell=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT)