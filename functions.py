#!/usr/bin/env python

# This module contains functions that is used by arcigo-assistant

from distro import id
import os
from os import path, getlogin, mkdir, makedirs, listdir
from os import rmdir, unlink, walk, execl, getpid, system, stat, readlink
import datetime
import subprocess
import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify


distro_name = id()
username = os.getlogin()
home_dir = os.path.expanduser("~")
home = "/home/" + str(username)
config = home_dir + "/.config/arcigo-assistant/settings.ini"
config_dir = home_dir + "/.config/arcigo-assistant/"
# pacman_config = "/etc/pacman.conf"
pacman_config = "./pacman.conf"
arcigo_mirrorlist = "/etc/pacman.d/arcigo-mirrorlist"

assistant_data_dir = "/usr/share/arcigo-linux/assistant"
arcigo_mirrorlist_original = assistant_data_dir + "/arcigo-mirrorlist"
pacman_eos = assistant_data_dir + "/data/eos/pacman.conf"
pacman_garuda = assistant_data_dir + "/data/garuda/pacman.conf"
blank_pacman_arco = assistant_data_dir + "/data/arco/blank/pacman.conf"
blank_pacman_eos = assistant_data_dir + "/data/eos/blank/pacman.conf"
blank_pacman_garuda = assistant_data_dir + "/data/garuda/blank/pacman.conf"
autostart = home_dir + "/.config/autostart/"
log_dir="/var/log/arcigo-linux/"
bd = ".assistant_backups"
config = home + "/.config/arcigo-assistant/settings.ini"
config_dir = home + "/.config/arcigo-assistant/"

# Send desktop notification
def show_notify(title, description, status):
    Notify.init("Hello world")
    Hello = Notify.Notification.new(title, description, status)
    Hello.show()

# Get Position
def _get_position(lists, value):
    data = [string for string in lists if value in string]
    position = lists.index(data[0])
    return position

# Create log file
def create_log():
    print('Making log in ' + str(log_dir))
    now = datetime.datetime.now()
    time = now.strftime("%Y-%m-%d-%H-%M-%S" )
    destination = log_dir + 'arcigo-assistant-log-' + time
    command = 'sudo pacman -Q > ' + destination
    subprocess.call(command,
                    shell=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT)

# Run as other user
def run_as_user(script):
    subprocess.call(["su - " + username + " -c " + script], shell=False)

# Check if file or path exists
def check_file(file):
    if os.path.isfile(file):
        return True
    return False

def check_path(path):
    if os.path.isdir(path):
        return True
    return False

# Check if the directory is empty or not
def is_empty_directory(path):
    if os.path.exists(path) and not os.path.isfile(path):
        if not os.listdir(path):
            # print("Empty directory")
            return True
        else:
            # print("Not empty directory")
            return False

# Check backups
def check_backups(now):
    if not path.exists(home + "/" + bd + "/Backup-" + now.strftime("%Y-%m-%d %H")):
        makedirs(home + "/" + bd + "/Backup-" + now.strftime("%Y-%m-%d %H"), 0o777)
        permissions(home + "/" + bd + "/Backup-" + now.strftime("%Y-%m-%d %H"))

# check if package is installed or not
def check_package_installed(package):  # noqa
    try:
        subprocess.check_output(
            "pacman -Qi " + package, shell=True, stderr=subprocess.STDOUT
        )
        # package is installed
        return True
    except subprocess.CalledProcessError:
        # package is not installed
        return False

# Check if service is active or not
def check_service(service):  # noqa
    try:
        command = "systemctl is-active " + service + ".service"
        output = subprocess.run(
            command.split(" "),
            check=True,
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        status = output.stdout.decode().strip()
        if status == "active":
            # print("Service is active")
            return True
        else:
            # print("Service is inactive")
            return False
    except Exception:
        return False